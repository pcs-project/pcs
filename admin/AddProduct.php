<?php
include('navbar.php');
include('../connection.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <title>New Product ~ PCS</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
</head>
<script>
    function noti(typ, text) {
        swal({
            position: 'bottom-end',
            width: 300,
            height: 200,
            type: typ,
            title: "",
            text: text,
            timer: 2000,
            showConfirmButton: false

        });
    };
</script>

<body>
    <!-- partial:index.partial.html -->

    <body class="fixed-nav sticky-footer bg-dark" id="page-top">
        <!-- Navigation-->
        <div class="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">My Dashboard</li>
                </ol>


                <div class="form-group row">

                    <div class="col-sm-8 ml-3">
                        <label class="my-1 mr-2" for="category">Product Category</label>
                        <select class="custom-select my-1 mr-sm-2" id="category">
                            <option selected>Select Product Category</option>
                            <option value="cbt">Cabinet</option>
                            <option value="gc">Graphics Card</option>
                            <option value="hdp">Headphone</option>
                            <option value="kb">Keyboard</option>
                            <option value="ltp">Laptop</option>
                            <option value="mon">Monitor</option>
                            <option value="mb">Motherboard</option>
                            <option value="mse">Mouse</option>
                            <option value="cpu">Processor</option>
                            <option value="psu">PSU</option>
                            <option value="ram">RAM</option>
                            <option value="sd">Storage Device</option>
                            <option value="cam">Webcam</option>
                        </select>

                        <br>
                        <br>
                        <form id="cbt" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Cabinet ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="inputEmail3" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="inputEmail3" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="inputEmail3" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="inputEmail3" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="inputEmail3" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="inputEmail3" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="number" name="qty" class="form-control" id="inputEmail3" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Type</label>
                                <div class="col-sm-5">
                                    <input type="text" name="type" class="form-control" id="inputEmail3" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Cooling Support</label>
                                <div class="col-sm-5">
                                    <textarea name="csupport" class="form-control" id="inputEmail3" placeholder=""></textarea>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Front IO</label>
                                <div class="col-sm-5">
                                    <textarea name="io" class="form-control" id="inputEmail3" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 col-form-label">Form Factor</label>
                                <div class="col-sm-5">
                                    <textarea name="factor" class="form-control" id="inputEmail3" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image2">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image3">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>

                        <!--Graphics Card Form -->


                        <form id="gc" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Graphics Card ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="number" name="qty" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memory" class="col-sm-4 col-form-label">Memory</label>
                                <div class="col-sm-5">
                                    <input type="text" name="type" class="form-control" id="memory" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="engc" class="col-sm-4 col-form-label">Engine Clock</label>
                                <div class="col-sm-5">
                                    <input type="text" name="csupport" class="form-control" id="engc" placeholder="">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="boostc" class="col-sm-4 col-form-label">Boost Clock</label>
                                <div class="col-sm-5">
                                    <input type="text" name="io" class="form-control" id="boostc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memc" class="col-sm-4 col-form-label">Memory Clock</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="memc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">Dimensions</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="dim" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>

                        <!--Headphones -->

                        <form id="hdp" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Headphone ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="number" name="qty" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memory" class="col-sm-4 col-form-label">Type</label>
                                <div class="col-sm-5">
                                    <input type="text" name="type" class="form-control" id="memory" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="engc" class="col-sm-4 col-form-label">Driver Size</label>
                                <div class="col-sm-5">
                                    <input type="text" name="csupport" class="form-control" id="engc" placeholder="">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="boostc" class="col-sm-4 col-form-label">Frequency Range</label>
                                <div class="col-sm-5">
                                    <input type="text" name="io" class="form-control" id="boostc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memc" class="col-sm-4 col-form-label">Wireless</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="memc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">Mic</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="dim" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">Noise Cancellation</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="dim" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">Impedance</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="dim" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>

                        <!--Keyboard -->

                        <form id="kb" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Keyboard ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="number" name="qty" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memory" class="col-sm-4 col-form-label">Switch Type</label>
                                <div class="col-sm-5">
                                    <input type="text" name="type" class="form-control" id="memory" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="engc" class="col-sm-4 col-form-label">Keyboard Layout</label>
                                <div class="col-sm-5">
                                    <input type="text" name="csupport" class="form-control" id="engc" placeholder="">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="boostc" class="col-sm-4 col-form-label">Interface</label>
                                <div class="col-sm-5">
                                    <input type="text" name="io" class="form-control" id="boostc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>



                        <!--Laptops -->

                        <form id="ltp" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Laptop ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="number" name="qty" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memory" class="col-sm-4 col-form-label">Processor</label>
                                <div class="col-sm-5">
                                    <input type="text" name="type" class="form-control" id="memory" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="engc" class="col-sm-4 col-form-label">Graphics</label>
                                <div class="col-sm-5">
                                    <input type="text" name="csupport" class="form-control" id="engc" placeholder="">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="boostc" class="col-sm-4 col-form-label">RAM</label>
                                <div class="col-sm-5">
                                    <input type="text" name="io" class="form-control" id="boostc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memc" class="col-sm-4 col-form-label">Screensize</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="memc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">Refresh Rate</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="dim" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">Storage</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="dim" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">OS</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="dim" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">Ports</label>
                                <div class="col-sm-5">
                                    <textarea name="factor" class="form-control" id="dim" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>


                        <!--Monitors -->

                        <form id="mon" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Monitor ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="number" name="qty" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memory" class="col-sm-4 col-form-label">Screen Size</label>
                                <div class="col-sm-5">
                                    <input type="text" name="type" class="form-control" id="memory" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="engc" class="col-sm-4 col-form-label">Resolution</label>
                                <div class="col-sm-5">
                                    <input type="text" name="csupport" class="form-control" id="engc" placeholder="">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="boostc" class="col-sm-4 col-form-label">Aspect Ratio</label>
                                <div class="col-sm-5">
                                    <input type="text" name="io" class="form-control" id="boostc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memc" class="col-sm-4 col-form-label">Display Ports</label>
                                <div class="col-sm-5">
                                    <textarea name="factor" class="form-control" id="memc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">Refresh Rate</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="dim" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>


                        <!--Motherboard -->

                        <form id="mb" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Motherboard ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="number" name="qty" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Chipset</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Socket</label>
                                <div class="col-sm-5">
                                    <input type="text" name="desc" class="form-control" id="desc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Memory Slots</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="kw" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Maximum Memory</label>
                                <div class="col-sm-5">
                                    <input type="number" name="qty" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memory" class="col-sm-4 col-form-label">Expansion Slots</label>
                                <div class="col-sm-5">
                                    <textarea name="type" class="form-control" id="memory" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="engc" class="col-sm-4 col-form-label">Storage Slots</label>
                                <div class="col-sm-5">
                                    <textarea name="csupport" class="form-control" id="engc" placeholder=""></textarea>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="boostc" class="col-sm-4 col-form-label">Internal IO</label>
                                <div class="col-sm-5">
                                    <textarea name="io" class="form-control" id="boostc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="memc" class="col-sm-4 col-form-label">Back IO</label>
                                <div class="col-sm-5">
                                    <textarea name="factor" class="form-control" id="memc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dim" class="col-sm-4 col-form-label">Form Factor</label>
                                <div class="col-sm-5">
                                    <input type="text" name="factor" class="form-control" id="dim" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>


                        <!--Mouse -->

                        <form id="mse" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Mouse ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">No.of Buttons</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Interface</label>
                                <div class="col-sm-5">
                                    <input type="text" name="desc" class="form-control" id="desc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">DPI</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="kw" placeholder="">
                                </div>
                            </div>


                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>

                        <!--Processor -->

                        <form id="cpu" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Processor ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Core Count</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Thread Count</label>
                                <div class="col-sm-5">
                                    <input type="text" name="desc" class="form-control" id="desc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Socket Type</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="kw" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Cache</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Base Clock</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="kw" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Boost Clock</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="kw" placeholder="">
                                </div>
                            </div>


                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>


                        <!--PSU -->

                        <form id="psu" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">PSU ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Capacity</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Efficiency</label>
                                <div class="col-sm-5">
                                    <input type="text" name="desc" class="form-control" id="desc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Modularity</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="kw" placeholder="">
                                </div>
                            </div>


                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>


                        <!--RAM -->

                        <form id="ram" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">RAM ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Size</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Speed</label>
                                <div class="col-sm-5">
                                    <input type="text" name="desc" class="form-control" id="desc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Configuration</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="kw" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>


                        <!--Storage Devices -->

                        <form id="sd" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Storage Device ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Capacity</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Interface</label>
                                <div class="col-sm-5">
                                    <input type="text" name="desc" class="form-control" id="desc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Type</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="kw" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>


                        <!--Webcam-->

                        <form id="cam" class="group" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="id" class="col-sm-4 col-form-label">Webcam ID</label>
                                <div class="col-sm-5">
                                    <input type="text" name="id" class="form-control" id="id" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-5">
                                    <textarea name="name" class="form-control" id="name" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="company" class="col-sm-4 col-form-label">Company</label>
                                <div class="col-sm-5">
                                    <input type="text" name="company" class="form-control" id="company" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-4 col-form-label">Price</label>
                                <div class="col-sm-5">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-5">
                                    <textarea name="desc" class="form-control" id="desc" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Keywords</label>
                                <div class="col-sm-5">
                                    <textarea name="keywords" class="form-control" id="kw" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="qty" class="col-sm-4 col-form-label">Quantity</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="qty" placeholder="">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="desc" class="col-sm-4 col-form-label">Mic</label>
                                <div class="col-sm-5">
                                    <input type="text" name="desc" class="form-control" id="desc" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kw" class="col-sm-4 col-form-label">Resolution</label>
                                <div class="col-sm-5">
                                    <input type="text" name="keywords" class="form-control" id="kw" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="exampleFormControlFile1">Choose Image 1</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 2</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose Image 3</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <input type="submit" class="btn btn-outline-primary float-right" value="ADD PRODUCT" name="cabinet">
                                </div>
                            </div>
                        </form>





                    </div>

                </div>





            </div>
        </div>

        <?php


        if (!empty($_POST['cabinet'])) {
            $id = 'cbt000000' . $_POST['id'];
            $name = $_POST['name'];
            $company = $_POST['company'];
            $price = $_POST['price'];
            $desc = $_POST['desc'];
            $keywords = $_POST['keywords'];
            $qty = $_POST['qty'];
            $type = $_POST['type'];
            $csupport = $_POST['csupport'];
            $io = $_POST['io'];
            $factor = $_POST['factor'];

            $query = "INSERT into cabinet_details (cbtid,cid,cbtName,cbtCompany,cbtPrice,cbtKeywords,cbtQuantity,cbtType,cbtCoolingSupport,cbtFrontIO,cbtFormFactor) VALUES ('$id','cbt','$name','$company',$price,'$keywords',$qty,'$type','$csupport','$io','$factor')";
            $result = mysqli_query($connect_db, $query);
            if ($result) {
                //  $sup = '../images/products/cabinet/'.{$id};
                mkdir("\\xampp\\htdocs\\pcs\\images\\products\\cabinet\\{$id}", 0777, true);
                echo "<script type='text/javascript'>noti('success','Product Added Successfully');</script>";
                echo "<meta http-equiv='refresh' content='2'>";

                for ($i = 1; $i <= 3; $i++) {
                    $fileTmpPath = $_FILES['image' . $i]['tmp_name'];
                    $fileName = $_FILES['image' . $i]['name'];
                    $fileNameCmps = explode(".", $fileName);
                    $fileExtension = strtolower(end($fileNameCmps));

                    $newFileName = "cbt" . $i . '.' . $fileExtension;

                    $uploadFileDir = "../images/products/cabinet/" . $id . "/";
                    $dest_path = $uploadFileDir . $newFileName;
                    $img = move_uploaded_file($fileTmpPath, $dest_path);
                }
            }
        }

        ?>

        <?php include('footer.php'); ?>
        <script>
            $(document).ready(function() {
                $('.group').hide();
                $('#category').change(function() {
                    $('.group').hide();
                    $('#' + $(this).val()).show();
                })
            });
        </script>
    </body>

</html>