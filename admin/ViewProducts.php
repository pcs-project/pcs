<?php
include('navbar.php');
include('../connection.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <title>Manage Products ~ PCS</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
    <!-- partial:index.partial.html -->

    <body class="fixed-nav sticky-footer bg-dark" id="page-top">
        <!-- Navigation-->
        <div class="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Manage Products</li>
                </ol>

                <?php
                $str = explode("000000", $_GET['cid']);
                $cid = $str[0];
                // die;
                ?>
                <div class="form-group row">

                    <div class="col-sm-12">
                        <label class="my-1 mr-2" for="category">Product Category</label>
                        <select class="custom-select my-1 mr-sm-2 col-sm-6" id="category" disabled>
                            <option selected>Select Product Category</option>
                            <option value="cbt" <?php if ($cid == 'cbt') {
                                                    echo 'selected';
                                                } ?>>Cabinet</option>
                            <option value="gc" <?php if ($cid == 'gc') {
                                                    echo 'selected';
                                                } ?>>Graphics Card</option>
                            <option value="hdp" <?php if ($cid == 'hdp') {
                                                    echo 'selected';
                                                } ?>>Headphone</option>
                            <option value="kb" <?php if ($cid == 'kb') {
                                                    echo 'selected';
                                                } ?>>Keyboard</option>
                            <option value="ltp" <?php if ($cid == 'ltp') {
                                                    echo 'selected';
                                                } ?>>Laptop</option>
                            <option value="mon" <?php if ($cid == 'mon') {
                                                    echo 'selected';
                                                } ?>>Monitor</option>
                            <option value="mb" <?php if ($cid == 'mb') {
                                                    echo 'selected';
                                                } ?>>Motherboard</option>
                            <option value="mse" <?php if ($cid == 'mse') {
                                                    echo 'selected';
                                                } ?>>Mouse</option>
                            <option value="cpu" <?php if ($cid == 'cpu') {
                                                    echo 'selected';
                                                } ?>>Processor</option>
                            <option value="psu" <?php if ($cid == 'psu') {
                                                    echo 'selected';
                                                } ?>>PSU</option>
                            <option value="ram" <?php if ($cid == 'ram') {
                                                    echo 'selected';
                                                } ?>>RAM</option>
                            <option value="sd" <?php if ($cid == 'sd') {
                                                    echo 'selected';
                                                } ?>>Storage Device</option>
                            <option value="cam" <?php if ($cid == 'cam') {
                                                    echo 'selected';
                                                } ?>>Webcam</option>
                        </select>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>

        <?php


        if (!empty($_POST['cabinet'])) {


            $id = $_POST['id'];
            $name = $_POST['name'];
            $company = $_POST['company'];
            $price = $_POST['price'];
            $desc = $_POST['desc'];
            $keywords = $_POST['keywords'];
            $qty = $_POST['qty'];
            $type = $_POST['type'];
            $csupport = $_POST['csupport'];
            $io = $_POST['io'];
            $factor = $_POST['factor'];

            $query = "INSERT into cabinet_details (cbtid,cid,cbtName,cbtCompany,cbtPrice,cbtKeywords,cbtQuantity,cbtType,cbtCoolingSupport,cbtFrontIO,cbtFormFactor) VALUES ('$id','cbt','$name','$company',$price,'$keywords',$qty,'$type','$csupport','$io','$factor')";
            $result = mysqli_query($connect_db, $query);
            if ($result) {
                //  $sup = '../images/products/cabinet/'.{$id};
                mkdir("\\xampp\\htdocs\\pcsg\\images\\products\\cabinet\\{$id}", 0777, true);
                echo '<script>
            alert("New Cabinet Added Successfully")
        </script>';
                echo "<meta http-equiv='refresh' content='0'>";


                $fileTmpPath = $_FILES['image1']['tmp_name'];
                $fileName = $_FILES['image1']['name'];
                $fileNameCmps = explode(".", $fileName);
                $fileExtension = strtolower(end($fileNameCmps));

                $newFileName = "cbt1" . '.' . $fileExtension;

                $uploadFileDir = "../images/products/cabinet/" . $id . "/";
                $dest_path = $uploadFileDir . $newFileName;

                if (move_uploaded_file($fileTmpPath, $dest_path)) {

                    echo '<script>alert("file Successfully")</script>';
                } else {
                    $err = $_FILES["file"]["error"];
                    echo '<script>alert($err)</script>';
                }
            }
        }

        ?>

        <?php include('footer.php'); ?>
        <script>
            $(document).ready(function() {
                // $('.group').hide();
                // $('.group').hide();
                $('#' + $('#category').val()).show();
            });
        </script>

        <script>
            $(".show-more a").on("click", function() {
                var $this = $(this);
                var $content = $this.parent().prev("div.content");
                var linkText = $this.text().toUpperCase();

                if (linkText === "SHOW MORE") {
                    linkText = "Show less";
                    $content.switchClass("hideContent", "showContent", 100);
                } else {
                    linkText = "Show more";
                    $content.switchClass("showContent", "hideContent", 100);
                };

                $this.text(linkText);
            });
        </script>
    </body>

</html>