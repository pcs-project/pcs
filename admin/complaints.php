<?php
include('navbar.php');

use PHPMailer\PHPMailer\PHPMailer;

include('../connection.php');
require_once "PHPMailer/PHPMailer.php";
require_once "PHPMailer/SMTP.php";
require_once "PHPMailer/Exception.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <title>Complaints ~ PCS</title>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap4.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
  <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
</head>
<script>
  function noti(typ, text) {
    swal({
      position: 'bottom-end',
      width: 300,
      height: 200,
      type: typ,
      title: "",
      text: text,
      timer: 2000,
      showConfirmButton: false

    });
  };
</script>

<body>
  <!-- partial:index.partial.html -->

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Response Model -->
    <div class="modal fade" id="responsemodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Complaint Response</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form action="" method="POST">

            <div class="modal-body">
              <div class="form-group">
                <label>Customer Name</label>
                <input type="text" name="ucid" class="form-control" id="ucid" hidden>
                <input type="text" name="cname" value="names" class="form-control" id="cname" readonly>
              </div>

              <div class="form-group">
                <label>Customer Email</label>
                <input type="text" name="cemail" value="emails" class="form-control" id="cemail" readonly>
              </div>

              <div class="form-group">
                <label> Response </label>
                <textarea rows="6" name="response" id="response" class="form-control" placeholder="Enter Response"></textarea>
              </div>


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <input type="submit" value="Send" class="btn btn-primary" name="SendBtn" />
            </div>
          </form>

        </div>
      </div>
    </div>

    <!-- Delete Model -->

    <div class="modal fade" id="deletemodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete Complaint</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form action="" method="POST">

            <div class="modal-body">
              <div class="alert alert-danger" role="alert">
                Are you sure you want to delete this complaint ?
              </div>
              <div class="form-group">
                <label>Customer Name</label>
                <input type="text" name="cid" class="form-control" id="cid" hidden>
                <input type="text" name="dname" value="names" class="form-control" id="dname" readonly>
              </div>

              <div class="form-group">
                <label>Customer Email</label>
                <input type="text" name="demail" value="emails" class="form-control" id="demail" readonly>
              </div>

              <div class="form-group">
                <label> Complaint </label>
                <textarea rows="6" name="dresponse" id="dresponse" value="dresponse" class="form-control" readonly></textarea>
              </div>


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancel</button>
              <input type="submit" value="Delete" class="btn btn-outline-danger" name="DelBtn" />

            </div>
          </form>

        </div>
      </div>
    </div>

    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Complaints</li>
        </ol>


        <div class="card mb-3">
          <div class="card-header">
            <i class="fa fa-exclamation-circle" style="color:red" aria-hidden="true"></i> Pending Complaints
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <?php
              // $connection = mysqli_connect("localhost", "root", "");
              // $db = mysqli_select_db($connection, 'test');


              $query = "SELECT * FROM Complaints WHERE ComplaintStatus = 'Pending'";
              $query_run = mysqli_query($connect_db, $query);
              ?>
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Customer ID</th>
                    <th>Customer Name</th>
                    <th>Customer Email</th>
                    <th>Status</th>
                    <th>Complaint</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <?php
                if ($query_run) {
                  foreach ($query_run as $row) {
                ?>
                    <tbody>
                      <tr>
                        <td> <?php echo $row['ID']; ?> </td>
                        <td> <?php echo $row['CustomerID']; ?> </td>
                        <td> <?php echo $row['CustomerName']; ?> </td>
                        <td> <?php echo $row['CustomerEmail']; ?> </td>
                        <td> <?php echo $row['ComplaintStatus']; ?> </td>
                        <td> <?php echo $row['ComplaintDesc']; ?> </td>
                        <td>
                          <button type="button" class="btn btn-outline-success btn-sm responsebtn" data-toggle="modal" data-target="#responsemodal"><i class="fa fa-check"></i></button>
                          <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>

                        </td>

                      </tr>


                    </tbody>
                <?php
                  }
                } else {
                  echo "No Record Found";
                }
                ?>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">
            <?php echo "Last Updated " . date("Y-m-d h:i:sa"); ?>
          </div>
        </div>





        <div class="card mb-3">
          <div class="card-header">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Complaints History
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <?php
              // $connection = mysqli_connect("localhost", "root", "");
              // $db = mysqli_select_db($connection, 'test');


              $query = "SELECT * FROM Complaints WHERE ComplaintStatus = 'Resolved'";
              $query_run = mysqli_query($connect_db, $query);
              ?>
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Customer ID</th>
                    <th>Customer Name</th>
                    <th>Customer Email</th>
                    <th>Status</th>
                    <th>Complaint</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <?php
                if ($query_run) {
                  foreach ($query_run as $row) {
                ?>
                    <tbody>
                      <tr>
                        <td> <?php echo $row['ID']; ?> </td>
                        <td> <?php echo $row['CustomerID']; ?> </td>
                        <td> <?php echo $row['CustomerName']; ?> </td>
                        <td> <?php echo $row['CustomerEmail']; ?> </td>
                        <td> <?php echo $row['ComplaintStatus']; ?> </td>
                        <td> <?php echo $row['ComplaintDesc']; ?> </td>
                        <td>
                          <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>

                        </td>

                      </tr>


                    </tbody>
                <?php
                  }
                } else {
                  echo "No Record Found";
                }
                ?>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">
            <?php echo "Last Updated " . date("Y-m-d h:i:sa"); ?>
          </div>
        </div>


      </div>

    </div>
    <?php include('footer.php'); ?>
  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.5/umd/popper.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap4.js"></script>
  <script src="vendor/js/script.js"></script>
  <script>
    $(document).ready(function() {
      $('.responsebtn').on('click', function() {

        $('#responsemodel').modal('show');


        $tr = $(this).closest('tr');

        var data = $tr.children("td").map(function() {
          return $(this).text();
        }).get();

        // console.log(data);



        //document.getElementById("cname").setAttribute("value",data[2]);
        //document.getElementById("cemail").setAttribute("value",data[3]);

        // $('#cname').val(data[2]);
        //$('#cemail').val(data[3]);

        $('#ucid').attr('value', data[0]);
        $('#cname').attr('value', data[2]);
        $('#cemail').attr('value', data[3]);



      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('.delbtn').on('click', function() {

        $('#deletemodel').modal('show');


        $tr = $(this).closest('tr');

        var data = $tr.children("td").map(function() {
          return $(this).text();
        }).get();

        console.log(data);



        //document.getElementById("cname").setAttribute("value",data[2]);
        //document.getElementById("cemail").setAttribute("value",data[3]);

        // $('#cname').val(data[2]);
        //$('#cemail').val(data[3]);
        $('#cid').attr('value', data[0]);
        $('#dname').attr('value', data[2]);
        $('#demail').attr('value', data[3]);
        //$('#dresponse').attr('value',data[5]);
        $('#dresponse').val(data[5]);



      });
    });
  </script>

</html>
<?php

if (isset($_POST['SendBtn'])) {



  // $connection = mysqli_connect("localhost", "root", "");
  // $db = mysqli_select_db($connection, 'test');



  $ucid = $_POST['ucid'];
  $subject = "Complaint Resolved";
  $responsecontent = $_POST['response'];
  $email = $_POST['cemail'];
  $name = "PCS";

  $query = "UPDATE complaints SET ComplaintStatus = 'Resolved' WHERE id ='$ucid'  ";

  $query_run = mysqli_query($connect_db, $query);

  if ($query_run) {
    echo "<script type='text/javascript'>noti('success','Complaint Status Changed to Resolved');</script>";
    $mail = new PHPMailer();

    //SMTP Settings
    $mail->isSMTP();
    $mail->Host = "smtp.gmail.com";
    $mail->SMTPAuth = true;
    $mail->Username = "hotelorbitbaroda@gmail.com";
    $mail->Password = 'hotelorbitrocks';
    $mail->Port = 465;
    $mail->SMTPSecure = "ssl";

    //Email Settings
    $mail->isHTML(true);
    $mail->setFrom($email, $name);
    $mail->addAddress($email);
    $mail->Subject = ("$email ($subject)");
    $mail->Body = $responsecontent;

    if ($mail->send()) {
      $status = "success";
      $response = "Email is sent!";
    } else {
      $status = "failed";
      $response = "Something is wrong: <br><br>" . $mail->ErrorInfo;
    }

    // exit(json_encode(array("status" => $status, "response" => $response)));

  } else {
    echo "<script type='text/javascript'>noti('error','Error sending mail...please try again');</script>";
  }
}
if (isset($_POST['DelBtn'])) {



  // $connection = mysqli_connect("localhost", "root", "");
  // $db = mysqli_select_db($connection, 'test');


  $did = $_POST['cid'];
  $query = "DELETE FROM Complaints WHERE id='$did'";
  $query_run = mysqli_query($connect_db, $query);

  if ($query_run) {
    echo "<script type='text/javascript'>noti('success','Complaint Deleted Successfully');</script>";
    //header("Location:index.php");
  } else {
    echo "<script type='text/javascript'>noti('error','Error while deleting complaint...please try again');</script>";
  }
}
?>