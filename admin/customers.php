<?php include('../connection.php');
  include('navbar.php');
if (isset($_POST['DelBtn'])) {

  $did = $_POST['userid'];
  $query = "DELETE FROM user_details WHERE userid='$did'";
  $query_run = mysqli_query($connect_db, $query);

  if ($query_run) {
    echo '<script> alert("User Account Deleted..."); </script>';
    //header("Location:index.php");
  } else {
    echo '<script> alert("Error While Deleting User...Please Try Again"); </script>';
  }
} ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <title>Customers ~ PCS</title>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap4.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
  <link rel="stylesheet" href="css/style.css" />
</head>


<!-- partial:index.partial.html -->

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Delete Model -->

  <div class="modal fade" id="deletemodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form action="" method="POST">

          <div class="modal-body">
            <div class="alert alert-danger" role="alert">
              Are you sure you want to delete this user ?
            </div>
            <div class="form-group">
              <label>Customer ID</label>
              <input type="text" name="userid" value="names" class="form-control" id="userid" readonly>
            </div>

            <div class="form-group">
              <label>Customer Name</label>
              <input type="text" name="uname" value="emails" class="form-control" id="uname" readonly>
            </div>

            <div class="form-group">
              <label>Customer Email </label>
              <input type="text" name="uemail" value="emails" class="form-control" id="uemail" readonly>
            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancel</button>
            <input type="submit" value="Delete" class="btn btn-outline-danger" name="DelBtn" />

          </div>
        </form>

      </div>
    </div>
  </div>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Customers</li>
      </ol>


      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-fw fa-users"></i> Customers
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <?php
            // $connection = mysqli_connect("localhost", "root", "");
            // $db = mysqli_select_db($connection, 'test');

            $query = "SELECT * FROM user_details";
            $query_run = mysqli_query($connect_db, $query);
            ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Customer ID</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>DOB</th>
                  <th>Contact</th>
                  <th>Address</th>
                  <th>City</th>
                  <th>State</th>
                  <th>Country</th>
                </tr>
              </thead>
              <?php
              if ($query_run) {
                foreach ($query_run as $row) {
              ?>
                  <tbody>
                    <tr>
                      <td> <?php echo $row['userid']; ?> </td>
                      <td> <?php echo $row['userFirstName']; ?> </td>
                      <td> <?php echo $row['userLastName']; ?> </td>
                      <td> <?php echo $row['userEmail']; ?> </td>
                      <td> <?php echo $row['userDOB']; ?> </td>
                      <td> <?php echo $row['userContact']; ?> </td>
                      <td> <?php echo $row['userAddress']; ?> </td>
                      <td> <?php echo $row['userCity']; ?> </td>
                      <td> <?php echo $row['userState']; ?> </td>
                      <td> <?php echo $row['userCountry']; ?> </td>
                      <td>

                        <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>

                      </td>

                    </tr>


                  </tbody>
              <?php
                }
              } else {
                echo "No Record Found";
              }
              ?>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">
          <?php echo "Last Updated " . date("Y-m-d h:i:sa"); ?>
        </div>
      </div>






    </div>

  </div>
  <?php include('footer.php'); ?>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.5/umd/popper.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap4.js"></script>
<script src="vendor/js/script.js"></script>
<script>
  $(document).ready(function() {
    $('.delbtn').on('click', function() {

      $('#deletemodel').modal('show');


      $tr = $(this).closest('tr');

      var data = $tr.children("td").map(function() {
        return $(this).text();
      }).get();

      console.log(data);



      //document.getElementById("cname").setAttribute("value",data[2]);
      //document.getElementById("cemail").setAttribute("value",data[3]);

      // $('#cname').val(data[2]);
      //$('#cemail').val(data[3]);
      $('#userid').attr('value', data[0]);
      $('#uname').attr('value', data[1]);
      $('#uemail').attr('value', data[3]);
      //$('#dresponse').attr('value',data[5]);
      $('#dresponse').val(data[5]);



    });
  });
</script>

</html>