<?php
include('navbar.php');
include('../connection.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <title>Dashboard ~ PCS</title>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap4.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
  <link rel="stylesheet" href="css/style.css" />
</head>

<body>
  <!-- partial:index.partial.html -->

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Navigation-->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">My Dashboard</li>
        </ol>
        <!-- Icon Cards-->
        <div class="row">
          <?php
          $query = "SELECT * FROM order_details WHERE orderStatus='Pending'";
          $result = mysqli_query($connect_db, $query);
          $newOrders = mysqli_num_rows($result);


          $query = "SELECT * FROM complaints WHERE ComplaintStatus='Pending'";
          $result = mysqli_query($connect_db, $query);
          $newComplaints = mysqli_num_rows($result);

          $query = "SELECT * FROM user_details ";
          $result = mysqli_query($connect_db, $query);
          $totalUsers = mysqli_num_rows($result);


          ?>
          <div class="col-xl-4 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-fw fa-shopping-cart"></i>
                </div>
                <div class="mr-5"><?php echo $newOrders ?> New Orders!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="pendingorders.php">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>

          <div class="col-xl-4 col-sm-6 mb-3">
            <div class="card text-white bg-danger o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-exclamation-circle"></i>
                </div>
                <div class="mr-5"><?php echo $newComplaints ?> New Complaints!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="complaints.php">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-4 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-users"></i>
                </div>
                <div class="mr-5"><?php echo $totalUsers ?> Total Customers</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="customers.php">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>

          <div class="col-sm-8">

            <hr>
            <ol class="breadcrumb">

              <li class="breadcrumb-item active">Latest Orders</li>
            </ol>
            <table class="table table-striped">
              <?php



              $query = "select * from order_details order by orderid DESC Limit 5";
              $query_run = mysqli_query($connect_db, $query);
              ?>
              <thead class="thead-dark">

                <tr>
                  <th scope="col">ID</th>

                  <th scope="col">Product</th>
                  <th scope="col">Quantity</th>
                  <th scope="col">Amount</th>
                  <th scope="col">Date</th>
                </tr>
              </thead>
              <?php
              if ($query_run) {
                foreach ($query_run as $row) {
              ?>
                  <tbody>
                    <tr>
                      <td><?php echo $row['orderid']; ?></td>
                      <td><?php echo $row['product_name']; ?></td>
                      <td><?php echo $row['quantity']; ?></td>
                      <td><?php echo $row['price']; ?></td>
                      <td><?php echo $row['order_date']; ?></td>

                    </tr>
                  </tbody>
              <?php
                }
              } else {
                echo "No Record Found";
              }
              ?>
            </table>

          </div>



          <div class="col-sm-4">
            <hr>
            <div class="card-header">
              <i class="fa fa-bell-o"></i> New Complaints</div>
            <div class="list-group list-group-flush small">
              <?php


              $records = mysqli_query($connect_db, "select * from complaints order by id desc limit 4 ");

              while ($data = mysqli_fetch_array($records)) {
              ?>

                <div class="list-group-item list-group-item-action">
                  <div class="media">
                    <img class="d-flex mr-3 rounded-circle" src="https://img.icons8.com/color/36/000000/complaint.png" alt="">

                    <div class="media-body">
                      <strong><?php echo $data['CustomerName']; ?> : </strong><?php echo $data['ComplaintDesc']; ?><br>
                      <p class="font-weight-light">Status - <?php echo $data['ComplaintStatus']; ?></p>
                      <div class="text-muted smaller">Complaint ID : <?php echo $data['ID']; ?></div>
                    </div>
                  </div>
                </div>

              <?php
              }
              ?>



              <a class="list-group-item list-group-item-action" href="complaints.php">View all Complaints</a>
            </div>
            <div class="card-footer small text-muted"> <?php echo "Last Updated " . date("Y-m-d h:i:sa"); ?></div>
          </div>


        </div>
      </div>
      <!-- /.container-fluid-->
      <!-- /.content-wrapper-->
      <?php include('footer.php'); ?>
  </body>

</html>