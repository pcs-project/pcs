<?php session_start(); ?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>PCS ~ Login</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="icon" href="../images/logo.png">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<style>
  .body-bg {
    background-image: radial-gradient(circle, #2d3956, #455372);
    background-attachment: fixed;
  }

  .login-form {
    max-width: 450px;
    padding: 3% 5%;
    margin: auto;
    margin-top: 5%;
    border-radius: 10px;
    background-color: #e0e9e9;
    /* color: white; */
  }

  .form-input {
    border: 1px solid #e2efef;
    border-radius: 5px;
    width: 100%;
  }

  /* .btn {
    border: none;
    border-radius: 3px;
    font-size: 1.1em;
    cursor: pointer;
    color: white;
  } */

  /* .submit {
    background-color: #4CAF50;
  } */
</style>

<body class="body-bg">

  <?php
  include "../connection.php";
  if(isset($_SESSION['admin'])){
    session_destroy();
  }
  if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    if ($email == 'admin@pcs.com' && $password == '1234') {
      $_SESSION['admin'] = $email;
      header('Location: dashboard.php');
    } else {
      echo '<script>alert("Incorrect Username or Password")</script>';
    }
  }
  ?>
  <div class="login-form shadow">
    <h1 style="text-align: center;">LOGIN</h1>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="login">
      <div>
        <div class="row mt-5">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Email:</div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><input type="email" class="form-input shadow" name="email" value="admin@pcs.com" required /></div>
        </div>

        <div class="row mt-2">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Password:</div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><input type="password" class="form-input shadow" value="1234" name="password" required /></div>
        </div>

        <div>
          <div class="p-5 text-center"><input class="btn btn-success" name="submit" type="submit" value="LOGIN" /></div>
        </div>



      </div>
    </form>
    <p class="text-center">Not registered yet? <a href='registration.php'>Register Here</a></p>
  </div>



</body>

</html>