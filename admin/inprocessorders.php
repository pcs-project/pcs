<?php
include('../connection.php');
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <title>Orders ~ PCS</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="css/style.css" />
</head>
<script>
    function noti(typ, text) {
        swal({
            position: 'bottom-end',
            width: 300,
            height: 200,
            type: typ,
            title: "",
            text: text,
            timer: 2000,
            showConfirmButton: false

        });
    };
</script>

<body>
    <!-- partial:index.partial.html -->

    <body class="fixed-nav sticky-footer bg-dark" id="page-top">
        <!-- Response Model -->
        <div class="modal fade" id="approvemodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change Order Status</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>


                    <form action="" method="POST">

                        <div class="modal-body">
                            <div class="alert alert-primary" role="alert">
                                Change Order Status to <b>Completed </b> ? <br>
                                Are you sure you want to mark this order as complete ?
                            </div>
                            <div class="form-group">
                                <input type="text" name="oid" class="form-control" id="oid" hidden>
                            </div>
                        </div>
                        <div class=" modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <input type="submit" value="Change" class="btn btn-primary" name="approvebtn" />
                        </div>
                    </form>

                </div>
            </div>
        </div>




        <!-- Navigation-->
        <?php
        include('navbar.php');
        ?>
        <div class="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Orders - In Process</li>
                </ol>


                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fa fa-shopping-cart" style="color:blue" aria-hidden="true"></i> Orders - In Process
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php

                            $query = "SELECT * FROM order_details WHERE orderStatus = 'Processing'";
                            $query_run = mysqli_query($connect_db, $query);
                            ?>
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Cust. ID</th>
                                        <th>Order ID</th>
                                        <th>Product ID</th>
                                        <th>Product Name</th>
                                        <th>Qty</th>
                                        <th>Order Date</th>
                                        <th>Est. Delivery Date</th>
                                        <th>Customer Email</th>
                                        <th>Customer Contact</th>
                                        <th>Customer Address</th>
                                        <th>Price</th>
                                        <th>Modify</th>
                                    </tr>
                                </thead>
                                <?php
                                if ($query_run) {
                                    foreach ($query_run as $row) {
                                ?>
                                        <tbody>
                                            <tr>
                                                <td> <?php echo $row['userid']; ?> </td>
                                                <td> <?php echo $row['orderid']; ?> </td>
                                                <td> <?php echo $row['productid']; ?> </td>
                                                <td> <?php echo $row['product_name']; ?> </td>
                                                <td> <?php echo $row['quantity']; ?> </td>
                                                <td> <?php echo $row['order_date']; ?> </td>
                                                <td> <?php echo $row['delivery_date']; ?> </td>
                                                <td> <?php echo $row['userEmail']; ?> </td>
                                                <td> <?php echo $row['userContact']; ?> </td>
                                                <td> <?php echo $row['userAddress']; ?> </td>
                                                <td> <?php echo $row['price']; ?> </td>


                                                <td>
                                                    <button type="button" class="btn btn-outline-success btn-sm approvebtn" data-toggle="modal" data-target="#responsemodal"><i class="fa fa-check-circle" aria-hidden="true"></i></button>


                                                </td>

                                            </tr>


                                        </tbody>
                                <?php
                                    }
                                } else {
                                    echo "No Record Found";
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer small text-muted">
                        <?php echo "Last Updated " . date("Y-m-d h:i:sa"); ?>
                    </div>
                </div>

            </div>

        </div>
        <?php include('footer.php'); ?>
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.5/umd/popper.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap4.js"></script>
    <script src="vendor/js/script.js"></script>
    <script>
        $(document).ready(function() {
            $('.approvebtn').on('click', function() {

                $('#approvemodel').modal('show');


                $tr = $(this).closest('tr');

                var data = $tr.children("td").map(function() {
                    return $(this).text();
                }).get();

                //console.log(data);

                $('#oid').attr('value', data[1]);


            });
        });
    </script>


</html>

<?php
if (isset($_POST['approvebtn'])) {

    $oid = $_POST['oid'];
    $query = "UPDATE order_details SET orderStatus = 'Completed' WHERE orderid ='$oid'  ";
    $query_run = mysqli_query($connect_db, $query);
    if ($query_run) {
        echo "<script type='text/javascript'>noti('success','Order Completed Successfully');</script>";
        echo "<meta http-equiv='refresh' content='2'>";
    } else {
        echo "<script type='text/javascript'>noti('error','Error while changing status...please try again');</script>";
    }
}

?>