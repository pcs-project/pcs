<?php
include('navbar.php');
include('../connection.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <title>Manage Products ~ PCS</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
    <!-- partial:index.partial.html -->

    <body class="fixed-nav sticky-footer bg-dark" id="page-top">
        <!-- Navigation-->
        <div class="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Manage Products</li>
                </ol>


                <div class="form-group row">

                    <div class="col-sm-12">
                        <form type="POST" class="form">
                            <label class="my-1 mr-2" for="category">Product Category</label>
                            <select class="custom-select my-1 mr-sm-2 col-sm-6" id="category">
                                <option value="select" selected>Select Product Category</option>
                                <option value="cbt">Cabinet</option>
                                <option value="gc">Graphics Card</option>
                                <option value="hdp">Headphone</option>
                                <option value="kb">Keyboard</option>
                                <option value="ltp">Laptop</option>
                                <option value="mon">Monitor</option>
                                <option value="mb">Motherboard</option>
                                <option value="mse">Mouse</option>
                                <option value="cpu">Processor</option>
                                <option value="psu">PSU</option>
                                <option value="ram">RAM</option>
                                <option value="sd">Storage Device</option>
                                <option value="cam">Webcam</option>
                            </select>
                        </form>
                        <br>
                        <br>
                        <div class="group col-sm-12" style="display:none">
                            <div class="card-header"></div>
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        <th>Price</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th>Manage</th>
                                    </tr>
                                </thead>
                                <tbody id="cbt" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Cabinet_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="gc" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Graphics_Card_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="hdp" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Headphone_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="kb" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Keyboard_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="ltp" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM laptop_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="mon" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Monitor_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="mb" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Motherboard_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="mse" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Mouse_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="cpu" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Processor_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="psu" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM PSU_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="ram" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM RAM_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="sd" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Storage_device_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                                <tbody id="cam" class="tbody" style="display:none">
                                    <?php
                                    $query = "SELECT * FROM Webcam_details";
                                    $query_run = mysqli_query($connect_db, $query);
                                    if ($query_run) {
                                        foreach ($query_run as $row) {
                                            $id = $row['cid'];
                                    ?>
                                            <tr>
                                                <td> <?php echo $row[$id . 'id']; ?> </td>
                                                <td> <?php echo $row[$id . 'Name']; ?> </td>
                                                <td> <?php echo $row[$id . 'Company']; ?> </td>
                                                <td> <?php echo $row[$id . 'Price']; ?> </td>
                                                <td>
                                                    <div class="content hideContent">
                                                        <?php echo $row[$id . 'Description']; ?>
                                                    </div>
                                                    <div class="show-more">
                                                        <a href="#">Show more</a>
                                                    </div>
                                                </td>
                                                <td> <?php echo $row[$id . 'Quantity']; ?> </td>
                                                <td>
                                                    <a class="btn btn-outline-success btn-sm delbtn" href="/pcs/admin/viewproducts.php?cid=<?php echo $row[$id.'id'];?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-outline-danger btn-sm delbtn" data-toggle="modal" data-target="#deletemodal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "No Record Found";
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div class="card-footer small text-muted">
                                <?php echo "Last Updated " . date("Y-m-d h:i:sa"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('footer.php'); ?>
        <script>
            $(document).ready(function() {
                $('#category').change(function() {
                    $('.group').hide();
                    var category_name = $('#category option:selected').text();
                    if ($('#category option:selected').val() != 'select') {
                        $('.group').show();
                    }
                    $('.tbody').hide();
                    $('.card-header').text(category_name);
                    $('#' + $('#category option:selected').val()).show();
                })
            });
        </script>

        <script>
            $(".show-more a").on("click", function() {
                var $this = $(this);
                var $content = $this.parent().prev("div.content");
                var linkText = $this.text().toUpperCase();

                if (linkText === "SHOW MORE") {
                    linkText = "Show less";
                    $content.switchClass("hideContent", "showContent", 100);
                } else {
                    linkText = "Show more";
                    $content.switchClass("showContent", "hideContent", 100);
                };

                $this.text(linkText);
            });
        </script>
    </body>

</html>